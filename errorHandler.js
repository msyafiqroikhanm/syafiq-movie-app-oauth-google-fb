const multer = require("multer");
function errorHandler(err, req, res, next) {
  if (err.status) {
    res.status(err.status).json({ message: err.message });
  } else if (err instanceof multer.MulterError) {
    console.log(err);
    res.status(400).json({ message: err });
  } else {
    req.sentry.captureException(err);
    res.status(500).json({ message: "Internal Server Error" });
    console.log(err);
  }
}
module.exports = errorHandler;
