"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Movie extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Movie.hasMany(models.Wishlist, { foreignKey: "movieId", as: "Movie" });
      Movie.belongsTo(models.User, { foreignKey: "userId", as: "Created_by" });
    }
  }
  Movie.init(
    {
      userId: DataTypes.INTEGER,
      name: DataTypes.STRING,
      show_time: DataTypes.STRING,
      poster: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "Movie",
    }
  );
  return Movie;
};
