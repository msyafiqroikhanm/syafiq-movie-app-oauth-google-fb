"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    static associate(models) {
      // define association here
      User.hasMany(models.Wishlist, { foreignKey: "userId", as: "User" });
      User.belongsTo(models.Role, { foreignKey: "roleId", as: "Role" });
      User.hasMany(models.Movie, { foreignKey: "userId", as: "Created_by" });
    }
  }
  User.init(
    {
      roleId: DataTypes.INTEGER,
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      auth: DataTypes.STRING,
      photo_profile: DataTypes.STRING,
      forgot_pass_token: DataTypes.STRING,
      forgot_pass_token_expiredAt: DataTypes.DATE,
    },
    {
      sequelize,
      modelName: "User",
    }
  );
  return User;
};
