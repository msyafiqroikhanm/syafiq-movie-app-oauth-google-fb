const multer = require("multer");
const fs = require("fs");

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    if (!fs.existsSync("public/src")) {
      fs.mkdirSync("public/src", { recursive: true });
    }
    cb(null, "public/src");
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + "-" + file.fieldname + "-" + file.originalname);
  },
});

module.exports = storage;
