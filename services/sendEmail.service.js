const nodemailer = require("nodemailer");
require("dotenv").config();

module.exports = async (from, to, html, text, subject) => {
  const transporter = nodemailer.createTransport({
    host: "smtp.mailtrap.io",
    port: 2525,
    auth: {
      user: "3a192db96fab90",
      pass: "cd0cb7b39a8506",
    },
  });

  let info = await transporter.sendMail({
    from,
    to,
    subject,
    text,
    html,
  });

  return info;
};
