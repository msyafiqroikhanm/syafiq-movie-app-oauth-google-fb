"use strict";
const bcrypt = require("bcryptjs");

module.exports = {
  async up(queryInterface, Sequelize) {
    const salt = bcrypt.genSaltSync(10);
    const hash = bcrypt.hashSync("syafiq", salt);
    await queryInterface.bulkInsert("Users", [
      {
        roleId: 1,
        email: "superadmin@mail.com",
        password: hash,
        auth: "local",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        roleId: 2,
        email: "admin@mail.com",
        password: hash,
        auth: "local",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        roleId: 3,
        email: "syafiq@mail.com",
        password: hash,
        auth: "local",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    queryInterface.bulkDelete("Users", null, {
      truncate: true,
      restartIdentity: true,
    });
  },
};
