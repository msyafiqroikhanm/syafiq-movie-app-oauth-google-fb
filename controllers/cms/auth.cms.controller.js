const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { User } = require("../../models");
const cloudinary = require("../../services/cloudinary.service");
const Op = require("sequelize").Op;
const otpGenerator = require("otp-generator");
const fs = require("fs");
const sendEmailService = require("../../services/sendEmail.service");
require("dotenv").config();
const role = { superAdmin: 1, admin: 2 };

class AuthController {
  static async login(req, res, next) {
    try {
      let token;
      if (req.body.email && req.body.password) {
        const user = await User.findOne({
          where: { email: req.body.email, [Op.not]: [{ roleId: 3 }] },
        });
        if (!user) {
          throw {
            status: 401,
            message: "Invalid email or password!",
          };
        }
        if (!bcrypt.compareSync(req.body.password, user.password)) {
          throw {
            status: 401,
            message: "Invalid email or password",
          };
        }

        token = jwt.sign(
          { id: user.id, roleId: user.roleId, email: user.email },
          process.env.JWT_SECRET_KEY
          // { expiresIn: "1h" }
        );
      } else {
        throw {
          status: 404,
          message: "Invalid email or password",
        };
      }
      res.status(200).json({ token });
    } catch (error) {
      next(error);
    }
  }

  static async register(req, res, next) {
    try {
      const adminRoleId = 2;

      console.log(req.file);
      const user = await User.findOne({
        where: {
          email: req.body.email,
          auth: "local",
        },
      });
      if (user) {
        throw {
          status: 409,
          message: "User already exist",
        };
      }

      req.body.password = bcrypt.hashSync(req.body.password, 10);
      req.body.auth = "local";
      req.body.roleId = adminRoleId;

      if (req.file) {
        const fullPath = req.file.path.split("\\");
        req.body.photo_profile = `http://localhost:3000/${fullPath[2]}`;
        // const cdn = await cloudinary.uploader.upload(req.file.path);
        // req.body.photo_profile = cdn.secure_url;
        // fs.unlinkSync(req.file.path);
      }

      User.create(req.body);
      res.status(201).json({ message: "Successfully register user" });
      await sendEmailService(
        "admin@movie.com",
        req.body.email,
        `Selamat Datang di CMS Movie App wahay <b>${req.body.email}</b>`,
        null,
        "Welcome to CMS Movie App"
      );
    } catch (error) {
      next(error);
    }
  }

  static async authorizationSuperAdmin(req, res, next) {
    try {
      //cek auth token
      if (!req.headers.authorization) {
        throw {
          status: 401,
          message: "Unauthorized request",
        };
      }

      //cek payload
      const payload = jwt.verify(
        req.headers.authorization,
        process.env.JWT_SECRET_KEY
      );
      if (!payload) {
        throw {
          status: 401,
          message: "Unauthorized request",
        };
      }

      //cek user role superadmin
      const loggedInUser = await User.findOne({
        where: { email: payload.email, roleId: role.superAdmin },
      });
      if (!loggedInUser) {
        throw {
          status: 401,
          message: "Unauthorized request",
        };
      }
      req.user = payload;
      next();
    } catch (error) {
      next(error);
    }
  }

  static async authorizationAdmin(req, res, next) {
    try {
      //cek auth token
      if (!req.headers.authorization) {
        throw {
          status: 401,
          message: "Unauthorized request a",
        };
      }

      //cek payload
      const payload = jwt.verify(
        req.headers.authorization,
        process.env.JWT_SECRET_KEY
      );
      console.log(payload);
      if (!payload) {
        throw {
          status: 401,
          message: "Unauthorized request b",
        };
      }

      //cek user role admin
      const loggedInUser = await User.findOne({
        where: {
          email: payload.email,
          [Op.not]: [{ roleId: 3 }],
        },
      });
      if (!loggedInUser) {
        throw {
          status: 401,
          message: "Unauthorized request c",
        };
      }
      req.user = payload;
      next();
    } catch (error) {
      next(error);
    }
  }

  static async sendForgotPassToken(req, res, next) {
    try {
      const user = await User.findOne({
        where: {
          email: req.body.email,
          auth: "local",
          [Op.not]: [{ roleId: 3 }],
        },
      });
      if (!user) {
        throw {
          status: 404,
          message: "Email not registered",
        };
      }

      const otp = otpGenerator.generate(6, {
        upperCaseAlphabets: false,
        specialChars: false,
      });
      const salt = bcrypt.genSaltSync(10);
      const hash = bcrypt.hashSync(otp, salt);
      const expired = new Date(new Date().getTime() + 5 * 60000);

      await User.update(
        {
          forgot_pass_token: hash,
          forgot_pass_token_expiredAt: expired,
        },
        {
          where: {
            email: req.body.email,
          },
        }
      );

      res.status(200).json({ message: "Please check your e-mail" });
      await sendEmailService(
        "admin@movie.com",
        req.body.email,
        `Email: ${req.body.email} , Token: ${otp}`,
        null,
        "Forgot Password Movie App"
      );
    } catch (error) {
      next(error);
    }
  }

  static async verifyForgotPassToken(req, res, next) {
    try {
      const user = await User.findOne({
        where: {
          email: req.body.email,
          auth: "local",
          [Op.not]: [{ roleId: 3 }],
        },
      });
      if (!user) {
        throw {
          status: 404,
          message: "Invalid email or token",
        };
      }

      if (!bcrypt.compareSync(req.body.token, user.forgot_pass_token)) {
        throw {
          status: 401,
          message: "Invalid email or token",
        };
      }

      if (new Date() > user.forgot_pass_token_expiredAt) {
        throw {
          status: 401,
          message: "Your Token is Expired",
        };
      }

      res.status(200).json({ valid: true, message: "Your Token is Valid" });
    } catch (error) {
      next(error);
    }
  }

  static async changePassword(req, res, next) {
    try {
      const user = await User.findOne({
        where: {
          email: req.body.email,
          auth: "local",
          [Op.not]: [{ roleId: 3 }],
        },
      });
      if (!user) {
        throw {
          status: 404,
          message: "Invalid email or token",
        };
      }

      if (!bcrypt.compareSync(req.body.token, user.forgot_pass_token)) {
        throw {
          status: 404,
          message: "Invalid email or token",
        };
      }

      if (new Date() > user.forgot_pass_token_expiredAt) {
        throw {
          status: 401,
          message: "Token is expired",
        };
      }

      if (req.body.password !== req.body.password_confirm) {
        throw {
          status: 400,
          message: "Password confirmation doesnt match",
        };
      }

      const salt = bcrypt.genSaltSync(10);
      const hashedPassword = bcrypt.hashSync(req.body.password, salt);

      await User.update(
        {
          password: hashedPassword,
          forgot_pass_token: null,
          forgot_pass_token_expiredAt: null,
        },
        {
          where: { id: user.id },
        }
      );

      res.status(200).json({ message: "Successfully update password" });
    } catch (error) {
      next(error);
    }
  }
}
module.exports = AuthController;
