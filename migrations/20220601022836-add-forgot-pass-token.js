const { Sequelize, DataTypes } = require("sequelize");
("use strict");

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.addColumn("Users", "forgot_pass_token", {
      type: DataTypes.STRING,
    });
    await queryInterface.addColumn("Users", "forgot_pass_token_expiredAt", {
      type: DataTypes.DATE,
    });
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.removeColumn("Users", "forgot_pass_token", null);
    await queryInterface.removeColumn(
      "Users",
      "forgot_pass_token_expiredAt",
      null
    );
  },
};
