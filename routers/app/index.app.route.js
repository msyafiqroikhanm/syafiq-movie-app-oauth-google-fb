const router = require("express").Router();
const AuthController = require("../../controllers/app/auth.app.controller");
const MovieController = require("../../controllers/app/movie.controller");
const wishlistRoutes = require("./wishlist.route");
const { check, validationResult } = require("express-validator");
const storage = require("../../services/multerStorage.service");
const multer = require("multer");
const upload = multer({
  storage,
  limits: { fileSize: 3000000 },
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype === "image/jpg" ||
      file.mimetype === "image/jpeg" ||
      file.mimetype === "image/png"
    ) {
      cb(null, true);
    } else {
      cb({ status: 400, message: "File should be an image" }, false);
    }
  },
});

router.post("/login", AuthController.login);

router.post(
  "/register",

  upload.single("photo_profile"),

  [(check("email").not().isEmpty(), check("password").not().isEmpty())],
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      throw {
        status: 400,
        message: errors,
      };
    }
    next();
  },

  AuthController.register
);

router.post("/forgot-password", AuthController.sendForgotPassToken);
router.post(
  "/verify-forgot-password-token",
  AuthController.verifyForgotPassToken
);
router.post("/change-password", AuthController.changePassword);
router.get("/movies", MovieController.moviesToBeShown);
router.use("/wishlist", wishlistRoutes);

module.exports = router;
