const router = require("express").Router();
const { check, validationResult } = require("express-validator");
const AuthController = require("../../controllers/cms/auth.cms.controller");
const movieRoutes = require("./movie.route");
const storage = require("../../services/multerStorage.service");
const multer = require("multer");
const upload = multer({
  storage,
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype === "image/jpg" ||
      file.mimetype === "image/jpeg" ||
      file.mimetype === "image/png"
    ) {
      cb(null, true);
    } else {
      cb(new Error("File should be an image"), false);
    }
  },
  limits: {
    fileSize: 3000000,
  },
});

router.post(
  "/register",

  AuthController.authorizationSuperAdmin,

  upload.single("photo_profile"),

  [(check("email").not().isEmpty(), check("password").not().isEmpty())],
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      throw {
        status: 400,
        message: errors,
      };
    }
    next();
  },

  AuthController.register
);

router.post("/forgot-password", AuthController.sendForgotPassToken);
router.post(
  "/verify-forgot-password-token",
  AuthController.verifyForgotPassToken
);
router.post("/change-password", AuthController.changePassword);

router.post("/login", AuthController.login);
router.use("/movies", movieRoutes);

module.exports = router;
