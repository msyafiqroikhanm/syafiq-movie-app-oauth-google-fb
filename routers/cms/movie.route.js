const router = require("express").Router();
const AuthController = require("../../controllers/cms/auth.cms.controller");
const MovieController = require("../../controllers/cms/movie.controller");
const multer = require("multer");
const storage = require("../../services/multerStorage.service");
const upload = multer({
  storage,
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype === "image/jpg" ||
      file.mimetype === "image/jpeg" ||
      file.mimetype === "image/png"
    ) {
      cb(null, true);
    } else {
      cb(new Error("File should be an image"), false);
    }
  },
  limits: {
    fileSize: 3000000,
  },
});

router.get("/", AuthController.authorizationAdmin, MovieController.getAll);
router.post(
  "/",
  AuthController.authorizationAdmin,

  upload.single("poster"),

  (req, res, next) => {
    let errors = [];
    if (!req.body.name) {
      errors.push("name is required");
    }
    if (!req.body.show_time) {
      errors.push("show_time is required");
    }
    if (new Date(req.body.show_time) < new Date()) {
      errors.push("show_time should be greather than today");
    }
    if (errors.length !== 0) {
      next({
        status: 400,
        message: errors,
      });
    }
    next();
  },

  MovieController.add
);
router.get("/:id", AuthController.authorizationAdmin, MovieController.getById);
router.put(
  "/:id",
  AuthController.authorizationAdmin,

  upload.single("poster"),

  (req, res, next) => {
    let errors = [];
    if (new Date(req.body.show_time) < new Date()) {
      errors.push("show_time should be greather than today");
    }
    if (errors.length !== 0) {
      next({
        status: 400,
        message: errors,
      });
    }
    next();
  },

  MovieController.update
);
router.delete(
  "/:id",
  AuthController.authorizationAdmin,
  MovieController.delete
);

module.exports = router;
